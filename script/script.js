var links = document.querySelectorAll(".links"),
	btnBack = document.querySelector(".back"),
	mainBlock = document.querySelector(".main_block"),
	bigBlock = document.querySelectorAll(".big_block");


function moveLeft() {
	for (var i = 0; i < bigBlock.length; i++) {
		if(bigBlock[i].style.width = 100 + "%") {
			bigBlock[i].style.width = 0 + "%";
		};
	};

	
	document.documentElement.style.overflow = "auto";
	document.body.style.background = "#BCBCBCFF";
};

for (var i = 0; i < links.length; i++) {
	(function(i){
		links[i].addEventListener('click', function(){
			mainBlock.style.transitionDelay = 0.2 + 's';
			mainBlock.style.transform = 'scale(0, 0)';			
			document.documentElement.style.overflow = "hidden"; 
			bigBlock[i].style.width = 100 + '%';
		});
	})(i);
};


btnBack.addEventListener('click', function(event) {
	moveLeft();
	mainBlock.style.transitionDelay = 0.5 + 's';
	mainBlock.style.transform = 'scale(1, 1)';
});